﻿using static InferenceLibrary.Severity;

namespace InferenceLibrary
{
    public class ModelRuleProcessor
    {        
        public Severity ActivateRule(FuzzyTuple fuzzyTuple) =>
            (fuzzyTuple.ApplicationGrade.Key, fuzzyTuple.PresentationGrade.Key, fuzzyTuple.ArrangementGrade.Key) switch
            {
                //If ML is %% and CS is %% and CS2 is %% then ADIN is %%
                (VeryBad, VeryBad, VeryBad) => VeryBad,         //1
                (VeryBad, VeryBad, Bad) => VeryBad,           //2
                (VeryBad, VeryBad, Good) => VeryBad,       //3
                (VeryBad, VeryBad, Excellent) => VeryBad,         //4
                (VeryBad, Bad, VeryBad) => VeryBad,           //5
                (VeryBad, Good, VeryBad) => VeryBad,       //6
                (VeryBad, Excellent, VeryBad) => VeryBad,         //7
                (VeryBad, Bad, Bad) => VeryBad,             //8
                (VeryBad, Bad, Good) => VeryBad,         //9
                (VeryBad, Bad, Excellent) => VeryBad,           //10
                (VeryBad, Good, Bad) => VeryBad,         //11
                (VeryBad, Excellent, Bad) => VeryBad,           //12
                (VeryBad, Good, Good) => VeryBad,     //13
                (VeryBad, Good, Excellent) => Bad,         //14
                (VeryBad, Excellent, Good) => Bad,         //15
                (VeryBad, Excellent, Excellent) => Bad,           //16
                (Bad, VeryBad, VeryBad) => VeryBad,           //17
                (Bad, VeryBad, Bad) => VeryBad,             //18
                (Bad, VeryBad, Good) => Bad,           //19
                (Bad, VeryBad, Excellent) => Bad,             //20
                (Bad, Bad, VeryBad) => VeryBad,             //21
                (Bad, Good, VeryBad) => Bad,           //22
                (Bad, Excellent, VeryBad) => Bad,             //23
                (Bad, Bad, Bad) => Bad,                 //24
                (Bad, Bad, Good) => Bad,             //25
                (Bad, Bad, Excellent) => Good,           //26
                (Bad, Good, Bad) => Bad,             //27
                (Bad, Excellent, Bad) => Good,           //28
                (Bad, Good, Good) => Good,     //29
                (Bad, Good, Excellent) => Good,       //30
                (Bad, Excellent, Good) => Good,       //31
                (Bad, Excellent, Excellent) => Good,         //32
                (Good, VeryBad, VeryBad) => Bad,         //33
                (Good, VeryBad, Bad) => Bad,           //34
                (Good, VeryBad, Good) => Good,   //35
                (Good, VeryBad, Excellent) => Good,     //36
                (Good, Bad, VeryBad) => Bad,           //37
                (Good, Good, VeryBad) => Bad,       //38
                (Good, Excellent, VeryBad) => Good,     //39
                (Good, Bad, Bad) => Bad,             //40
                (Good, Bad, Good) => Good,     //41
                (Good, Bad, Excellent) => Excellent,         //42
                (Good, Good, Bad) => Good,     //43
                (Good, Excellent, Bad) => Excellent,         //44
                (Good, Good, Good) => Good, //45
                (Good, Good, Excellent) => Excellent,     //46
                (Good, Excellent, Good) => Excellent,     //47
                (Good, Excellent, Excellent) => Excellent,       //48
                (Excellent, VeryBad, VeryBad) => Good,       //49
                (Excellent, VeryBad, Bad) => Good,         //50
                (Excellent, VeryBad, Good) => Excellent,       //51
                (Excellent, VeryBad, Excellent) => Excellent,         //52
                (Excellent, Bad, VeryBad) => Good,         //53
                (Excellent, Good, VeryBad) => Excellent,       //54
                (Excellent, Excellent, VeryBad) => Excellent,         //55
                (Excellent, Bad, Bad) => Good,           //56
                (Excellent, Bad, Good) => Excellent,         //57
                (Excellent, Bad, Excellent) => Excellent,           //58
                (Excellent, Good, Bad) => Excellent,         //59
                (Excellent, Excellent, Bad) => Excellent,           //60
                (Excellent, Good, Good) => Excellent,     //61
                (Excellent, Good, Excellent) => Excellent,       //62
                (Excellent, Excellent, Good) => Excellent,       //63
                (Excellent, Excellent, Excellent) => Excellent,         //64
            };
    }
}
