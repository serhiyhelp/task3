﻿namespace InferenceLibrary
{
    public enum Severity
    {
        VeryBad,
        Bad,
        Good,
        Excellent
    }
}
